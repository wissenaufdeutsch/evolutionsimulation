import java.util.Random;

/**
 * Die Klasse Lebewesen modelliert ein Lebewesen der Simulation
 * 
 * @author M.Koopmans
 * @version 1.0
 */
public abstract class LEBEWESEN
{   
    /**
     * Unterscheidungskonstante für das weibliche Geschlecht.
     */
    public static final int WEIBLICH = 1;
    /**
     * Unterscheidungskonstante für das maennliche Geschlecht.
     */
    public static final int MAENNLICH = 2;
    /**
     * Unterscheidungskonstante für geschlechtslose Lebewesen.
     */
    public static final int ZWITTER = 3;
    
    /**
     * Position des Lebewesen
     */
    private int x, y;
    
    /**
     * Bestimmt welche LEBEWESENDARSTELLUNG das LEBEWESEN hat.
     */
    private LEBEWESENDARSTELLUNG darstellung;
    
    /**
     * Das maximale Alter in "Ticks"
     */
    private int maxAlter;

    /**
     * Chance To Reproduce (0<ctr<1)
     */
    private double ctr;
    
    /**
     * Zufallsgenerator
     */
    protected Random generator = new Random();
    
    private int anzahlPotentiellerNachkommen;

    public LEBEWESENDARSTELLUNG gibAnzeige()
    {
        return this.darstellung;
    }
    
    public void setzeAnzeige(LEBEWESENDARSTELLUNG l)
    {
        darstellung = l;
    }
    
    /**
     * Setzt das maximale Alter in Ticks des Lebewesen.
     */
    void maxAlterSetzen(int max)
    {
        maxAlter = max;
    }
    
    /**
     * @return 
     */
    boolean pflanztSichFort()
    {
        return true;
        //return generator.nextDouble()<=ctr;
    }
    
    void positionSetzen(int xNeu, int yNeu)
    {
        this.x = xNeu;
        this.y = yNeu;
    }

    /**
     * Bestimmt, wie sich ein Lebewesen fortpflanzt. Hängt vom Lebewesen ab und muss daher überschrieben werden.
     * Gibt als Eingangsparameter die Anzahl mit, wie viele Versuche schon im aktuellen Wurf versucht wurden.
     */
    abstract LEBEWESEN fortpflanzen(int wurfNummer, int xPos, int yPos);
    
    /**
     * Muss in der Unterklasse überschrieben werden. Manche Lebewesen bewegen sich nicht z.B. GRAS!
     */
    abstract void bewegen();

    /**
     * Gibt die Wahrscheinlichkeit aus für die Reproduktion.
     */
    void setzeFortpflanzungsrate(double f)
    {
        if(f<=1)
        {
            if(f>=0)
            {
                this.ctr = f;
            }
            else
            { 
                ctr = 0;
            }

        }
        else
        {
            ctr=1;
        }
    }

    /**
     * Gibt die Fortpflanzungsrate aus.
     */
    double gebeFortpflanzungsrate()
    {
        return this.ctr;
    }
    
    /**
     * @return Gibt die x-Position des Lebewesen aus.
     */
    int gibXPosition()
    {
        return x;
    }
    
    /**
     * @return Gibt die y-Position des Lebewesen aus.
     */
    int gibYPosition()
    {
        return y;
    }
    
    void setzePotentielleNachkommen(int pn)
    {
        this.anzahlPotentiellerNachkommen = pn;
    }
    
    int gibMaxNachkommen()
    {
        return anzahlPotentiellerNachkommen;
    }
    
}
