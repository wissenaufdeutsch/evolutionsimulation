
/**
 * Enthält die Spiellogik.
 * 
 * @author M.Koopmans, M. Steigerwald
 * @version 1.0
 */
public class GOL extends GAMELOOP
{
    private WELT w;
    private VIEW v;

    /**
     * Konstruktor für Objekte der Klasse GOL
     */
    public GOL()
    {
        w = new WELT();
        v = new VIEW(w);
        Starten();
    }
    
    void TaktImpulsAusfuehren()
    {
        w.fortplanzungsZyklus();
        v.allesAnzeigen();
    }
}
