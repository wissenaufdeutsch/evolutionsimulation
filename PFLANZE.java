
/**
 * Eine Pflanze ist im wesentlichen ein Lebewesen, das sich nicht bewegt und fortpflanzt, ohne 
 * 
 * @author M.Koopmans
 * @version 1.0
 */
public abstract class PFLANZE extends LEBEWESEN
{
    /**
     * Pflanzen bewegen sich nicht!
     */
    void bewegen()
    {
        //Tut gar nichts
    }
    
}
