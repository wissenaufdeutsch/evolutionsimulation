import java.awt.*;
import javax. swing. *;
/**
 * Write a description of class GRASDARSTELLUNG here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class GRASDARSTELLUNG extends LEBEWESENDARSTELLUNG
{
    private int hGras;
    
    /**
     * Constructor for objects of class GRASDARSTELLUNG
     */
    public GRASDARSTELLUNG()
    {
        hGras = groesse-groesse/3;
        SetzeDarstellung();
    }

    public void SetzeDarstellung()
    {
        anzeige = new JComponent () {
            public void paintComponent (Graphics g)
            {
                g. clearRect (0, 0, groesse, groesse);
                g. setColor (Color.GREEN);
                g.drawLine(groesse/2, groesse, groesse/4, hGras);
                g.drawLine(groesse/2, groesse, groesse*3/4, hGras);
                g.drawLine(groesse/2, groesse, groesse/2, hGras);
            }

        };
    }
    
    public String GibLebewesenArt()
    {
        return "Gras";
    }
    
}
