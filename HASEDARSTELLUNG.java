import java.awt.*;
import javax. swing. *;
/**
 * Write a description of class HASEDARSTELLUNG here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class HASEDARSTELLUNG extends LEBEWESENDARSTELLUNG
{
    private int bHase;
    private int hHase;

    /**
     * Constructor for objects of class HASEDARSTELLUNG
     */
    public HASEDARSTELLUNG()
    {
        bHase = groesse/2;
        hHase = groesse*3/4;
        SetzeDarstellung();
    }

    public String GibLebewesenArt()
    {
        return "Hase";
    }
    
    public void SetzeDarstellung()
    {
        anzeige = new JComponent () {
            // Stellt das Rumpfelement auf dem Fenster dar.
            public void paintComponent (Graphics g)
            {
                g. clearRect (0, 0, groesse, groesse);
                g. setColor (Color.PINK);
                g.fillOval(groesse/4, groesse/4, bHase, hHase);
                g. setColor (Color.BLACK);
                g.drawOval(groesse/4, groesse/4, (groesse - 1)/2, (groesse - 1)*3/4);
                //g.drawOval(_x_, _y_, _width_, _height_)
                
            }

        };
    }
}
