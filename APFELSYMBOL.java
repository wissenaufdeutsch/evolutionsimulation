/**
 * Das Symbol eines Apfels.
 * Das Fenster wird bei Bedarf generiert.
 *
 * @author Albert Wiedemann
 * @version 1.0
 */

import java.awt.*;
import javax. swing. *;

class APFELSYMBOL
{
    /** Das Anzeigefenster. */
    private JFrame fenster;
    
    /** Anzeigegröße für den Apfel */
    private static final int groesse = OBERFLAECHE. RasterGroesseGeben ();

    /** Interna */
    private int x;
    private int y;
    private JComponent anzeige;

    /**
     * Standardkonstruktor für Objekte der Klasse KASSENSYMBOL.
     * Er erzeugt ein scharzes Rechteck in der linken oberen Ecke des Fensters.
     * Das Fenster wird bei Bedarf angelegt.
     */
    APFELSYMBOL()
    {
        fenster = OBERFLAECHE. FensterGeben ();
        anzeige = new JComponent () {
            // Stellt das Rumpfelement auf dem Fenster dar.
            public void paintComponent (Graphics g)
            {
                g. clearRect (1, 1, groesse - 1, groesse - 1);
                g. setColor (Color. white);
                g. fillRect (1, 1, groesse - 2, groesse - 2);
                g. setColor (Color. red);
                g. fillOval(groesse / 4, 3 * groesse / 8, groesse / 2, groesse / 2);
                g. setColor (Color. green);
                g. drawLine (groesse / 2 - 1, 3 * groesse / 8, 5 * groesse / 8 - 1, groesse / 8);
                g. drawLine (groesse / 2, 3 * groesse / 8, 5 * groesse / 8, groesse / 8);
                g. drawLine (groesse / 2 + 1, 3 * groesse / 8, 5 * groesse / 8 + 1, groesse / 8);
                g. drawLine (groesse / 2 - 1, 3 * groesse / 8, groesse / 4 - 1, groesse / 8);
                g. drawLine (groesse / 2, 3 * groesse / 8, groesse / 4, groesse / 8);
                g. drawLine (groesse / 2 + 1, 3 * groesse / 8, groesse / 4 + 1, groesse / 8);
                g. setColor (new Color (200, 100, 0));
                g. drawLine (groesse / 2 - 1, 3 * groesse / 8, 3 * groesse / 8 - 1, groesse / 8);
                g. drawLine (groesse / 2, 3 * groesse / 8, 3 * groesse / 8, groesse / 8);
                g. drawLine (groesse / 2 + 1, 3 * groesse / 8, 3 * groesse / 8 + 1, groesse / 8);
            }

        };
        anzeige. setVisible (true);
        anzeige. setSize (groesse, groesse);
        fenster. add (anzeige, 0);
        PositionSetzen (0, 0);
    }

    /**
     * Setzt die Position des Rumpfelements. Der Ursprung liegt in der Mitte des
     * Fensters, die y-Achse zeigt nach unten. (x /y) bedeutet das
     * K&auml;stchen rechts unterhalb der Gitterlinien.
     * @param x x-Position
     * @param y y-Position
     */
    public void PositionSetzen (int x, int y)
    {
        this. x = x;
        this. y = y;
        anzeige. setLocation (OBERFLAECHE. FensterBreiteGeben () / 2 + x * groesse, OBERFLAECHE. FensterHoeheGeben () / 2 + y * groesse);
    }

    /**
     * Gibt den X-Wert der Position des Kopfelements.
     * @return x-Position
     */
    int XPositionGeben ()
    {
        return x;
    }

    /**
     * Gibt den Y-Wert der Position des Kopfelements.
     * @return y-Position
     */
    int YPositionGeben ()
    {
        return y;
    }

    /**
     * Entfernt die Figur aus der Anzeige
     */
    public void Entfernen ()
    {
        (OBERFLAECHE. FensterGeben ()). remove (anzeige);
    }
}