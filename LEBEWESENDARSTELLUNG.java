import java.awt.*;
import javax. swing. *;

/**
 * 
 * 
 * @author M.Koopmans
 * @version 1.0
 */
public abstract class LEBEWESENDARSTELLUNG
{
    
    protected JComponent anzeige;
    
    protected int groesse = OBERFLAECHE.RasterGroesseGeben();
    /**
     * Gibt das JComponent zurück, das das Lebewesen der gegebenen Art darstellt.
     */
    public JComponent GibDarstellung()
    {
        return anzeige;
    }
    
    /**
     * Setzt die Darstellung für das Lebewesen. Muss überschrieben werden!
     */
    public abstract void SetzeDarstellung();
    
    /**
     * Gibt die Art des LEBEWESENS aus.
     */
    public abstract String GibLebewesenArt();
    
    
}
