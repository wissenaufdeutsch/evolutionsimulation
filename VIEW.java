import java.awt.*;
import javax. swing. *;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * VIEW zeichnet alle Elemente, die auf der Ausgabe angezeigt werden.
 * 
 * @author M.Koopmans
 * @version 1.0
 */
public class VIEW
{
    private WELT welt;

    /** Das Anzeigefenster. */
    private JFrame fenster;

    /** Anzeigegröße für das Rumpfelement */
    private static final int groesse = OBERFLAECHE. RasterGroesseGeben ();

    public VIEW()
    {
        fenster = OBERFLAECHE.FensterGeben();
        this.welt = new WELT();
        allesAnzeigen();
    }

    /**
     * Konstruktor für Objekte der Klasse VIEW
     */
    public VIEW(WELT w)
    {
        fenster = OBERFLAECHE. FensterGeben ();
        welt = w;
    }

    public void allesAnzeigen()
    {
        //TODO
        LEBEWESEN[][] t = welt.gibLebewesen();
        LEBEWESEN temp;
        for(int i = 0; i<t.length;i++)
        {
            for(int j = 0; j<t[i].length;j++)
            {
                if(t[i][j]!=null){
                temp = t[i][j];
                JComponent anz = temp.gibAnzeige().GibDarstellung();
                anz. setVisible (true);
                anz. setSize (groesse, groesse);
                fenster. add (anz, 0);
                anz.setLocation(OBERFLAECHE. FensterBreiteGeben () / 2 + temp.gibXPosition() * groesse, OBERFLAECHE. FensterHoeheGeben () / 2 + temp.gibYPosition() * groesse);
            }
            }
        }
    }

    private void AnzeigeHinzufuegen(String tier)
    {

    }

    /**
     * Setzt die Position des Rumpfelements. Der Ursprung liegt in der Mitte des
     * Fensters, die y-Achse zeigt nach unten. (x /y) bedeutet das
     * K&auml;stchen rechts unterhalb der Gitterlinien.
     * @param x x-Position
     * @param y y-Position
     */
    public void PositionSetzen (int x, int y)
    {
        //this. x = x;
        //this. y = y;
        //anzeige. setLocation (OBERFLAECHE. FensterBreiteGeben () / 2 + x * groesse, OBERFLAECHE. FensterHoeheGeben () / 2 + y * groesse);
    }

    /**
     * Entfernt die Figur aus der Anzeige
     */
    public void Entfernen (LEBEWESENDARSTELLUNG l)
    {
        (OBERFLAECHE. FensterGeben ()). remove (l.GibDarstellung());
    }
}
