

/**
 * Beschreiben Sie hier die Klasse GRAS.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class GRAS extends PFLANZE
{
    
    /**
     * Konstruktor für Objekte der Klasse GRAS
     */
    public GRAS()
    {
        super();
        setzeFortpflanzungsrate(0.5);
        setzePotentielleNachkommen(8);
        setzeAnzeige(new GRASDARSTELLUNG());
    }

    LEBEWESEN fortpflanzen(int i, int xPos, int yPos)
    {
        GRAS a = new GRAS();
        int x = xPos;
        int y = yPos;
        i = i%8;
        switch(i)
        {
            case 0:
            a.positionSetzen(x-1, y-1);
            break;
            case 1:
            a.positionSetzen(x, y-1);
            break;
            case 2:
            a.positionSetzen(x+1, y-1);
            break;
            case 3:
            a.positionSetzen(x-1, y);
            break;
            case 4:
            a.positionSetzen(x+1, y);
            break;
            case 5:
            a.positionSetzen(x-1, y+1);
            break;
            case 6:
            a.positionSetzen(x, y+1);
            break;
            case 7:
            a.positionSetzen(x+1, y+1);
            break;
            default:
            a.positionSetzen(x-1, y-1);
            break;
        }
        return a;
    }
    
    
    
}
