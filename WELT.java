import java.util.ArrayList;
import java.util.Iterator;

/**
 * Die Klasse WELT ist die Modell-Klasse nach dem Adapter Pattern, die alle Elemente der 
 * 
 * @author M.Koopmans
 * @version 1.0
 */
public class WELT
{
    //Alle Lebewesen gespeichert in einem Array
    private LEBEWESEN[][] life;
    private ArrayList<LEBEWESEN> newLife;
    public final int b = OBERFLAECHE.rasterXMax;
    public final int h = OBERFLAECHE.rasterYMax;
    /**
     * Konstruktor für Objekte der Klasse WELT
     */
    public WELT()
    {
        life = new LEBEWESEN[b][h];
        newLife = new ArrayList<LEBEWESEN>();
        init();
    }

    void init()
    {
        life[b/2][h/2] = new GRAS();
    }

    /**
     * Fügt ein Lebewesen in das Feld. Aber nur, wenn noch Platz ist.
     */
    public void lebewesenErschaffen(LEBEWESEN l)
    {
        if(l.gibXPosition()+b/2<b&&l.gibYPosition()+h/2<h&&l.gibXPosition()+b/2>0&&l.gibYPosition()+h/2>0)
        {
            if(life[l.gibXPosition()+b/2][l.gibYPosition()+h/2]==null){
                life[l.gibXPosition()+b/2][l.gibYPosition()+h/2] = l;
            }
        }
        else
        {
            System.out.println("God says: There is too much life in this world!");
        }
    }

    /**
     * Überprüft, welche Lebewesen sich fortpflanzen und erschafft die neuen Lebewesen.
     */
    public void fortplanzungsZyklus()
    {
        LEBEWESEN temp;
        for(int i = 0; i<b; i++)
        {
            for(int j = 0; j<h; j++)
            {
                if(life[i][j]!=null)
                {
                    temp = life[i][j];
                    for(int n = 0; n<temp.gibMaxNachkommen(); n++)
                    {
                        if(temp.pflanztSichFort())
                        {
                            LEBEWESEN newL = temp.fortpflanzen(n,i,j);
                            newLife.add(newL);
                            System.out.println("Neues Leben: " + temp.gibAnzeige().GibLebewesenArt() + " (" + temp.gibXPosition() + "," + temp.gibYPosition()); 
                        }
                    }
                }
            }
        }
        Iterator it;
        it = newLife.iterator();
        while(it.hasNext())
        {
            LEBEWESEN t = (LEBEWESEN) it.next();
            int xT = t.gibXPosition()+OBERFLAECHE.rasterXMax/2 - 1;
            int yT = t.gibYPosition()+OBERFLAECHE.rasterYMax/2 - 1;
            System.out.println(xT + " " + yT);
            if(life[xT][yT]!=null)
            {
                life[xT][yT] = t;
            }
            else
            {
                System.out.println("Hier lebt schon wer...");
                //TODO: Handle better (Foodchain?)
            }
        }
    }

    /**
     * Bewegt alle Lebewesen (die es können) nach ihren eigenen Fähigkeiten
     */
    public void bewege()
    {

    }

    public LEBEWESEN[][] gibLebewesen()
    {
        return life;
    }
}
